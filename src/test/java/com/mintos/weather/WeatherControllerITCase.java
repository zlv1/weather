package com.mintos.weather;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mintos.weather.config.TestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.lang.reflect.Type;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class WeatherControllerITCase {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestConfig testConfig;


    @Test
    void weather() throws Exception {
        String response = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build()
                .perform(get("/weather").with(replaceClientIp(testConfig.getMyIp())))
                .andDo(print()).andExpect(MockMvcResultMatchers.status().isOk()).andReturn()
                .getResponse().getContentAsString();
        Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
        Gson gson = new Gson();
        Map<String, Object> json = gson.fromJson(response, mapType);
        assertEquals("LV", json.get("country"));
        assertFalse(json.get("name").toString().isEmpty());
        Double temp = (Double) json.get("temp");
        assertTrue(temp < 40. && temp > -40.);
        Object humidity = json.get("humidity");
        assertNotNull(humidity);
        assertTrue((Double) humidity != 0.);
        assertNotNull(json.get("timestamp"));
    }

    private RequestPostProcessor replaceClientIp(String myIp) {
        return request -> {
            request.addHeader("X-FORWARDED-FOR", myIp);
            return request;
        };
    }
}