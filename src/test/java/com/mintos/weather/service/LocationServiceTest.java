package com.mintos.weather.service;

import com.mintos.weather.HttpClient;
import com.mintos.weather.config.ServerConfig;
import com.mintos.weather.response.LocationResponse;
import com.mintos.weather.repository.LocationRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.util.Optional;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = ServerConfig.class, properties = "spring.profiles.active=test,local")
class LocationServiceTest {

    public static final long TIMEOUT = 5000;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpClient httpClient;
    @Mock
    private LocationRepository locationRepository;
    @Autowired
    private ServerConfig serverConfig;

    public static String asString(Resource resource) throws IOException {
        try (Reader reader = new InputStreamReader(resource.getInputStream(), UTF_8)) {
            return FileCopyUtils.copyToString(reader);
        }
    }
    @Test
    void getLocationByIp() throws IOException, URISyntaxException, InterruptedException {
        LocationService locationService = new LocationService(httpClient, serverConfig, locationRepository);
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resourceIpApi = resourceLoader.getResource("classpath:geo_ip_response_ip_api.json");
        Resource resourceIpData = resourceLoader.getResource("classpath:geo_ip_response_ipdata.json");
        Resource resourceWrong = resourceLoader.getResource("classpath:geo_ip_response_wrong.json");

        // redirected case
        Mockito.when(httpClient.get("http://ip-api.com/json/1.1.1.1", TIMEOUT)).thenReturn(asString(resourceIpApi));
        Mockito.when(request.getHeader("X-FORWARDED-FOR")).thenReturn("1.1.1.1");
        getLocation(locationService, 1);

        // simple case
        Mockito.when(request.getHeader("X-FORWARDED-FOR")).thenReturn(null);
        Mockito.when(httpClient.get("http://ip-api.com/json/1.1.1.2", TIMEOUT)).thenReturn(asString(resourceIpApi));
        Mockito.when(request.getRemoteAddr()).thenReturn("1.1.1.2");
        getLocation(locationService, 2);

        // not available service 1 case
        Mockito.when(request.getRemoteAddr()).thenReturn("1.1.1.3");
        Mockito.when(httpClient.get("http://ip-api.com/json/1.1.1.3", TIMEOUT)).thenReturn(null);
        Mockito.when(httpClient.get("https://eu-api.ipdata.co/1.1.1.3?api-key=apkey", TIMEOUT)).thenReturn(asString(resourceIpData));
        getLocation(locationService, 3);

        // not available 2 services case
        Mockito.when(request.getRemoteAddr()).thenReturn("1.1.1.4");
        Optional<LocationResponse> locationResponseByIp = locationService.getLocationByIp(request);
        assertTrue(locationResponseByIp.isEmpty());
        Mockito.verify(locationRepository, Mockito.times(3)).save(Mockito.any());

        // wrong response data
        Mockito.when(request.getRemoteAddr()).thenReturn("1.1.1.5");
        Mockito.when(httpClient.get("http://ip-api.com/json/1.1.1.5", TIMEOUT)).thenReturn(asString(resourceWrong));
        locationResponseByIp = locationService.getLocationByIp(request);
        assertTrue(locationResponseByIp.isEmpty());
        Mockito.verify(locationRepository, Mockito.times(3)).save(Mockito.any());
    }

    private void getLocation(LocationService locationService, int invocations) {
        Optional<LocationResponse> locationById = locationService.getLocationByIp(request);
        assertTrue(locationById.isPresent());
        LocationResponse locationResponseByIp = locationById.get();
        assertEquals(56.94960021972656, locationResponseByIp.getLat(), 1E-5);
        assertEquals(24.09779930114746, locationResponseByIp.getLon(), 1E-5);
        assertEquals("Riga", locationResponseByIp.getCity());
        assertEquals("LV", locationResponseByIp.getCountryCode());
        Mockito.verify(locationRepository, Mockito.times(invocations)).save(Mockito.any());
    }
}