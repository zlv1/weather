package com.mintos.weather.service;

import com.mintos.weather.HttpClient;
import com.mintos.weather.config.ServerConfig;
import com.mintos.weather.entity.Location;
import com.mintos.weather.response.LocationResponse;
import com.mintos.weather.response.WeatherResponse;
import com.mintos.weather.repository.WeatherRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Optional;

import static com.mintos.weather.service.LocationServiceTest.asString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = ServerConfig.class, properties = "spring.profiles.active=test,local")
class WeatherServiceTest {

    public static final long TIMEOUT = 5000;
    @Mock
    private HttpClient httpClient;
    @Mock
    private WeatherRepository weatherRepository;
    @Mock
    private LocationService locationService;
    @Mock
    private HttpServletRequest request;
    @Autowired
    private ServerConfig serverConfig;

    @Test
    void getWeatherById() throws IOException, URISyntaxException, InterruptedException {
        WeatherService weatherService = new WeatherService(httpClient, serverConfig, weatherRepository, locationService);
        Mockito.when(locationService.getLocationByIp(request))
                .thenReturn(Optional.of(new LocationResponse(56.94960021972656, 24.09779930114746, "Riga", "LV", new Location())));
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resourceOpenweather = resourceLoader.getResource("classpath:weather_response_openweather.json");
        Resource resourceWeatherapi = resourceLoader.getResource("classpath:weather_response_weatherapi.json");

        // simple case
        Mockito.when(httpClient.get("https://api.openweathermap.org/data/2.5/weather?lat=56.949600&lon=24.097799&appid=apkey", TIMEOUT))
                .thenReturn(asString(resourceOpenweather));
        getWeather(weatherService);

        // not available service 1 case
        Mockito.when(locationService.getLocationByIp(request))
                .thenReturn(Optional.of(new LocationResponse(46.94960021972656, 14.09779930114746, "Riga", "LV", new Location())));
        Mockito.when(httpClient.get("https://api.openweathermap.org/data/2.5/weather?lat=46.949600&lon=14.097799&appid=apkey", TIMEOUT))
                .thenReturn(null);
        Mockito.when(httpClient.get("http://api.weatherapi.com/v1/current.json?q=46.949600,14.097799&aqi=no&key=apkey", TIMEOUT))
                .thenReturn(asString(resourceWeatherapi));
        getWeather(weatherService);

        // not available 2 services case
        Mockito.when(locationService.getLocationByIp(request))
                .thenReturn(Optional.of(new LocationResponse(36.94960021972656, 4.09779930114746, "Riga", "LV", new Location())));
        Mockito.when(httpClient.get("https://api.openweathermap.org/data/2.5/weather?lat=36.949600&lon=4.097799&appid=apkey", TIMEOUT))
                .thenReturn(null);
        Mockito.when(httpClient.get("http://api.weatherapi.com/v1/current.json?q=36.949600,4.097799&aqi=no&key=apkey", TIMEOUT))
                .thenReturn(null);
        Optional<WeatherResponse> weatherResponse = weatherService.getWeatherByIp(request);
        assertTrue(weatherResponse.isEmpty());
    }

    private void getWeather(WeatherService weatherService) {
        Optional<WeatherResponse> weatherByIp = weatherService.getWeatherByIp(request);
        assertTrue(weatherByIp.isPresent());
        WeatherResponse weatherResponse = weatherByIp.get();
        assertEquals("LV", weatherResponse.getCountry());
        assertEquals("Riga", weatherResponse.getName());
        assertEquals(23.32, weatherResponse.getTemp(), 1E-5);
        assertEquals(32, weatherResponse.getHumidity());
        assertNotEquals(0, weatherResponse.getTimestamp());
    }
}