package com.mintos.weather.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mintos.weather.HttpClient;
import com.mintos.weather.config.ServerConfig;
import com.mintos.weather.config.WeatherConfig;
import com.mintos.weather.entity.Weather;
import com.mintos.weather.response.LocationResponse;
import com.mintos.weather.response.WeatherResponse;
import com.mintos.weather.repository.WeatherRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@Component
public class WeatherService {
    static Logger logger = Logger.getLogger(WeatherService.class.getName());
    private final HttpClient httpClient;
    private final ServerConfig serverConfig;
    private final WeatherRepository weatherRepository;
    private final LocationService locationService;
    private final Cache<LocationResponse, Optional<WeatherResponse>> weatherResponses;

    public WeatherService(HttpClient httpClient, ServerConfig serverConfig, WeatherRepository weatherRepository, LocationService locationService) {
        this.httpClient = httpClient;
        this.serverConfig = serverConfig;
        this.weatherRepository = weatherRepository;
        this.locationService = locationService;

        weatherResponses = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(serverConfig.getWeatherCacheLifetime(), TimeUnit.SECONDS)
                .build();
    }

    public Optional<WeatherResponse> getWeatherByIp(HttpServletRequest request) {
        return locationService.getLocationByIp(request).flatMap(locationResponse -> {
            try {
                return weatherResponses.get(locationResponse, () -> getWeatherByLocation(locationResponse));
            } catch (ExecutionException e) {
                logger.severe("Unexpected error occured: " + e);
                return Optional.empty();
            }
        });
    }

    private static Object getMapElement(Map<String, Object> jsonWeather, String query) {
        return getMapElementInternal(jsonWeather, new ArrayList<>(Arrays.asList(query.split("/"))));
    }

    private static Object getMapElementInternal(Map<String, Object> jsonWeather, List<String> query) {
        Object result = jsonWeather.get(query.get(0));
        if (query.size() > 1) {
            if (!(result instanceof Map)) {
                return null;
            }
            query.remove(0);
            return getMapElementInternal((Map<String, Object>) result, query);
        }
        return result;
    }

    private Optional<WeatherResponse> getWeatherByLocation(LocationResponse locationResponse) {
        int weatherCount = 0;
        Exception lastException = null;
        String lastError = null;
        for (WeatherConfig weatherConfig: serverConfig.getWeather().values()) {
            ++weatherCount;
            try {
                String response = httpClient.get(String.format(weatherConfig.getUrl(),
                        locationResponse.getLat(), locationResponse.getLon(),
                        weatherConfig.getApiKey() != null ? weatherConfig.getApiKey() : ""), weatherConfig.getTimeout());
                Type mapType = new TypeToken<Map<String, Object>>() {}.getType();
                Gson gson = new Gson();
                Map<String, Object> json = gson.fromJson(response, mapType);
                String error = null;
                WeatherResponse weatherResponse = null;
                if (json == null) {
                    error = "response is empty";
                } else {
                    Object tempC = weatherConfig.getTempC() != null ? getMapElement(json, weatherConfig.getTempC())
                            : null;
                    Object tempK = weatherConfig.getTempK() != null ? getMapElement(json, weatherConfig.getTempK())
                            : null;
                    weatherResponse = new WeatherResponse((String) getMapElement(json, weatherConfig.getCountry()),
                            (String) getMapElement(json, weatherConfig.getName()),
                            tempC == null ? (tempK == null ? null : (Double) tempK - 273.15) : (Double) tempC,
                            (Double) getMapElement(json, weatherConfig.getHumidity()), new Date().getTime());
                    if (weatherResponse.getTemp() == null) {
                        error = "weather not specified in response";
                    }
                }
                if (error == null) {
                    Weather entity = new Weather(weatherResponse, locationResponse.getLocation());
                    weatherRepository.save(entity);
                    return Optional.of(weatherResponse);
                }
                lastError = error;
                if (serverConfig.getWeather().size() != weatherCount) {
                    logger.warning("Error occurred, trying another server: " + error);
                }
            } catch (URISyntaxException | IOException | InterruptedException e) {
                if (serverConfig.getWeather().size() != weatherCount) {
                    logger.warning("Exception occurred, trying another server: " + e);
                }
                lastException = e;
            }
        }
        if (lastException == null && lastError == null) {
            logger.severe("Weather config is empty");
        }
        if (lastException != null) {
            logger.warning("Exception occurred: " + lastException);
        }
        if (lastError != null) {
            logger.warning("Error occurred: " + lastError);
        }
        return Optional.empty();
    }
}
