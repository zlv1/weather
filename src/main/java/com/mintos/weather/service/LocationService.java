package com.mintos.weather.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mintos.weather.HttpClient;
import com.mintos.weather.config.GeoIpConfig;
import com.mintos.weather.entity.Location;
import com.mintos.weather.response.LocationResponse;
import com.mintos.weather.config.ServerConfig;
import com.mintos.weather.repository.LocationRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@Component
public class LocationService {
    static Logger logger = Logger.getLogger(LocationService.class.getName());
    private final HttpClient httpClient;
    private final ServerConfig serverConfig;
    private final LocationRepository locationRepository;
    private final Cache<String, Optional<LocationResponse>>  locationResponses;

    public LocationService(HttpClient httpClient, ServerConfig serverConfig, LocationRepository locationRepository) {
        this.httpClient = httpClient;
        this.serverConfig = serverConfig;
        this.locationRepository = locationRepository;

        locationResponses = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(serverConfig.getGeoipCacheLifetime(), TimeUnit.SECONDS)
                .build();
    }

    public static String getClientIpAddressIfServletRequestExist(HttpServletRequest request) {
        String ipList = request.getHeader("X-FORWARDED-FOR");
        if (ipList != null && ipList.length() != 0 && !"unknown".equalsIgnoreCase(ipList)) {
            return ipList.split(",")[0];
        }

        return request.getRemoteAddr();
    }

    public Optional<LocationResponse> getLocationByIp(HttpServletRequest request) {
        String clientIpAddress = getClientIpAddressIfServletRequestExist(request);
        try {
            return locationResponses.get(clientIpAddress, () -> {
                int geoipCount = 0;
                Exception lastException = null;
                String lastError = null;
                for (GeoIpConfig geoIpConfig : serverConfig.getGeoip().values()) {
                    ++geoipCount;
                    try {
                        String response = httpClient.get(String.format(geoIpConfig.getUrl(),
                                clientIpAddress,
                                geoIpConfig.getApiKey() != null ? "?api-key=" + geoIpConfig.getApiKey() : ""),
                                geoIpConfig.getTimeout());
                        Type mapType = new TypeToken<Map<String, Object>>() {
                        }.getType();
                        Gson gson = new Gson();
                        Map<String, Object> json = gson.fromJson(response, mapType);
                        String error = null;
                        Double lat = null;
                        Double lon = null;
                        String city = null;
                        if (json == null) {
                            error = "response is empty";
                        } else {
                            lat = (Double) json.get(geoIpConfig.getLatitude());
                            lon = (Double) json.get(geoIpConfig.getLongitude());
                            city = (String) json.get(geoIpConfig.getCity());
                            if ((lat == null || lon == null)
                                    && isNullOrEmpty(city)) {
                                error = "location not specified in response";
                            }
                        }
                        if (error == null) {
                            String countryCode = (String) json.get(geoIpConfig.getCountryCode());
                            Location location = locationRepository.save(new Location(lat, lon, city, countryCode,
                                    new Date().getTime(), clientIpAddress));
                            return Optional.of(new LocationResponse(lat, lon, city, countryCode, location));
                        }
                        lastError = error;
                        if (serverConfig.getGeoip().size() != geoipCount) {
                            logger.warning("Error occurred, trying another server: " + error);
                        }
                    } catch (URISyntaxException | IOException | InterruptedException e) {
                        if (serverConfig.getGeoip().size() != geoipCount) {
                            logger.warning("Exception occurred, trying another server: " + e);
                        }
                        lastException = e;
                    }
                }
                if (lastException == null && lastError == null) {
                    logger.severe("Geoip config is empty");
                }
                if (lastException != null) {
                    logger.warning("Exception occurred: " + lastException);
                }
                if (lastError != null) {
                    logger.warning("Error occurred: " + lastError);
                }
                return Optional.empty();
            });
        } catch (ExecutionException e) {
            logger.severe("Unexpected error occured: " + e);
            return Optional.empty();
        }
    }

    private boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }
}
