package com.mintos.weather.response;

public class WeatherResponse {
    private String country;
    private String name;
    private Double temp;
    private Double humidity;
    private final Long timestamp;

    public WeatherResponse(String country, String name, Double temp, Double humidity, Long timestamp) {
        this.country = country;
        this.name = name;
        this.temp = temp;
        this.humidity = humidity;
        this.timestamp = timestamp;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Long getTimestamp() {
        return timestamp;
    }
}
