package com.mintos.weather.response;

import com.google.common.base.Objects;
import com.mintos.weather.entity.Location;

public class LocationResponse {
    private final Double lat;
    private final Double lon;
    private final String city;
    private final String countryCode;
    private final Location location;

    public LocationResponse(Double lat, Double lon, String city, String countryCode, Location location) {
        this.lat = lat;
        this.lon = lon;
        this.city = city;
        this.countryCode = countryCode;
        this.location = location;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public String getCity() {
        return city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationResponse that = (LocationResponse) o;
        return Objects.equal(location.getId(), that.location.getId()) && Objects.equal(lat, that.lat) && Objects.equal(lon, that.lon) && Objects.equal(city, that.city) && Objects.equal(countryCode, that.countryCode);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(lat, lon, city, countryCode, location.getId());
    }
}
