package com.mintos.weather.repository;

import com.mintos.weather.entity.Location;
import com.mintos.weather.entity.Weather;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeatherRepository extends JpaRepository<Weather, String> {
}
