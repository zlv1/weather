package com.mintos.weather.entity;

import com.mintos.weather.response.WeatherResponse;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class Weather {
    private Double humidity;
    private String country;
    private Long timestamp;
    @ManyToOne
    private Location location;
    private Double temp;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Weather(WeatherResponse weatherResponse, Location location) {
        temp = weatherResponse.getTemp();
        humidity = weatherResponse.getHumidity();
        country = weatherResponse.getCountry();
        timestamp = weatherResponse.getTimestamp();
        this.location = location;
    }

    public Weather() {
    }

    public Double getHumidity() {
        return humidity;
    }

    public String getCountry() {
        return country;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public Location getLocation() {
        return location;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
