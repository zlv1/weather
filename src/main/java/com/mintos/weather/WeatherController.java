package com.mintos.weather;

import com.mintos.weather.response.WeatherResponse;
import com.mintos.weather.service.WeatherService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherController {

    private final WeatherService weatherService;

    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping(value = "/weather", produces = "application/json")
    public ResponseEntity<WeatherResponse> get(HttpServletRequest request) {
        return weatherService.getWeatherByIp(request)
                .map(ResponseEntity::ok).orElse(new ResponseEntity<>(HttpStatus.GATEWAY_TIMEOUT));
    }
}