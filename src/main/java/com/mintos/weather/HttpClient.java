package com.mintos.weather;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

@Component
public class HttpClient {
    public String get(String query, long timeout) throws URISyntaxException, IOException, InterruptedException {
        java.net.http.HttpClient httpClient = java.net.http.HttpClient.newBuilder()
                .connectTimeout(Duration.ofMillis(timeout)).build();
        return httpClient.send(HttpRequest.newBuilder()
                .uri(new URI(query))
                .GET()
                .build(), HttpResponse.BodyHandlers.ofString()).body();
    }
}
