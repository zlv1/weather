package com.mintos.weather.config;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.Map;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "server")
public class ServerConfig {
    private Long geoipCacheLifetime;
    private Long weatherCacheLifetime;
    private Map<String, GeoIpConfig> geoip;
    private Map<String, WeatherConfig> weather;

    public Map<String, GeoIpConfig> getGeoip() {
        return geoip;
    }

    public void setGeoip(Map<String, GeoIpConfig> geoip) {
        this.geoip = geoip;
    }

    public Map<String, WeatherConfig> getWeather() {
        return weather;
    }

    public void setWeather(Map<String, WeatherConfig> weather) {
        this.weather = weather;
    }

    public Long getGeoipCacheLifetime() {
        return geoipCacheLifetime;
    }

    public void setGeoipCacheLifetime(Long geoipCacheLifetime) {
        this.geoipCacheLifetime = geoipCacheLifetime;
    }

    public Long getWeatherCacheLifetime() {
        return weatherCacheLifetime;
    }

    public void setWeatherCacheLifetime(Long weatherCacheLifetime) {
        this.weatherCacheLifetime = weatherCacheLifetime;
    }
}
